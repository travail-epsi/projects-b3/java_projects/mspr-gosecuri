package com.staff;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class StaffTest {
  // @Test
  // public void testGetItems() {

  // }

  @Test
  public void testGetJob() {
    String initJob = "Detective";
    Staff s = new Staff("Sherlock", "Holmes", initJob, "W@ts0n", null);
    String getJob = s.getJob();
    assertEquals(initJob, getJob);
  }

  @Test
  public void testGetName() {
    String initName = "Sherlock";
    Staff s = new Staff(initName, "Holmes", "Detective", "W@ts0n", null);
    String getName = s.getName();
    assertEquals(initName, getName);
  }

  @Test
  public void testGetPassword() {
    String initPassword = "W@ts0n";
    Staff s = new Staff("Sherlock", "Holmes", "Detective", initPassword, null);
    String getPassword = s.getPassword();
    assertEquals(initPassword, getPassword);
  }

  @Test
  public void testGetSurname() {
    String initSurname = "Holmes";
    Staff s = new Staff("Sherlock", initSurname, "Detective", "W@ts0n", null);
    String getSurname = s.getSurname();
    assertEquals(initSurname, getSurname);
  }

  // @Test
  // public void testSetItems() {

  // }

  @Test
  public void testSetJob() {
    Staff s = new Staff("Sherlock", "Holmes", "Barman", "W@ts0n", null);
    String initJob = "Detective";
    s.setJob(initJob);
    String getJob = s.getJob();
    assertNotEquals(initJob, "Barman");
    assertEquals(initJob, getJob);
  }

  @Test
  public void testSetName() {
    Staff s = new Staff("John", "Holmes", "Barman", "W@ts0n", null);
    String initName = "Sherlock";
    s.setName(initName);
    String getName = s.getName();
    assertNotEquals(initName, "John");
    assertEquals(initName, getName);
  }

  @Test
  public void testSetPassword() {
    Staff s = new Staff("John", "Holmes", "Barman", "superSecretPassword", null);
    String initPassword = "W@ts0n";
    s.setPassword(initPassword);
    String getPassword = s.getPassword();
    assertNotEquals(initPassword, "superSecretPassword");
    assertEquals(initPassword, getPassword);
  }

  @Test
  public void testSetSurname() {
    Staff s = new Staff("Sherlock", "Watson", "Barman", "W@ts0n", null);
    String initSurname = "Holmes";
    s.setSurname(initSurname);
    String getSurname = s.getSurname();
    assertNotEquals(initSurname, "Watson");
    assertEquals(initSurname, getSurname);
  }
}
