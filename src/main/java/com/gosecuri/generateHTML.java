package com.gosecuri;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
//import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import com.staff.Staff;

public class generateHTML extends Thread {
  private Staff staff;

  public generateHTML() {
  }

  public generateHTML(Staff staff) {
    this.staff = staff;
  }

  @Override
  public void run() {
    String path = "output/webcontent/" + staff.getNickname() + "/" + staff.getNickname() + ".html";

    try {
      PrintWriter pWriter = new PrintWriter(path);

      pWriter.println("<head>");
      pWriter.println("<meta charset='UTF-8'>");
      pWriter.println(
          "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" crossorigin=\"anonymous\">");
      pWriter.println(
          "<link rel=\"stylesheet\" href=\"../css/style.css\">");
      pWriter.println("</head>");

      pWriter.println("<header>");

      pWriter
          .println("<div>" +
              "nom: " + staff.getSurname() + "</br>prénom: " + staff.getName() + "</br>poste: " + staff.getJob() +
              "</div>");

      pWriter.println("<img src='../images/" + staff.getNickname() + ".jpg'>");

      pWriter.println("</header>");

      pWriter.println("<footer>");
      BufferedReader buffer = new BufferedReader(new FileReader("assets/liste.txt"));

      while (buffer.ready()) {
        String line = buffer.readLine();
        String[] items = line.split("\t");
        String value = (staff.getItems().contains(items[0])) ? "checked" : "";

        pWriter.println("<div class='form-check'> " +
            "<input class='form-check-input' type='checkbox' id='" + items[0] + "' name='" + items[0] + "' " + value
            + " disabled>" +
            "<label class='form-check-label' for='" + items[0] + "'>" + items[1] + "</label>" +
            "</div>");

      }

      buffer.close();

      pWriter.println("</footer>");

      pWriter.close();

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public Staff getStaff() {
    return this.staff;
  }

  public void setStaff(Staff staff) {
    this.staff = staff;
  }

  public void createIndex(List<Staff> staffs) throws FileNotFoundException {
    String path = "output/webcontent/index.html";

    PrintWriter pWriter = new PrintWriter(path);
    pWriter.println(
        "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3\" crossorigin=\"anonymous\">");

    pWriter.println("<div class=\"table-responsive\">");
    pWriter.println("<table class=\"table table-striped\">\n" +
        "<thead>" +
        "<tr>" +
        "<th class=\"table-light\">Username</th>" +
        "<th class=\"table-light\">Page</th>" +
        "</tr>" +
        "</thead>");
    for (Staff s : staffs) {
      pWriter.println("<tr>" +
          "<td>" + s.getNickname() + "</td>" +
          "<td>" + "<a class=\"btn btn-primary\" href=\"" + s.getNickname() + "/" + s.getNickname() + ".html"
          + "\" role=\"button\">Link</a>" + "</td>" +
          "</tr>");
    }

    pWriter.println("</table>");
    pWriter.println("</div>");

    pWriter.close();

  }
}
