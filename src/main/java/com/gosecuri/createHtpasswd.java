package com.gosecuri;

//import java.io.BufferedReader;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.commons.codec.digest.Md5Crypt;

import com.staff.Staff;

public class createHtpasswd extends Thread {

  private Staff staff;

  public createHtpasswd(Staff staff) {
    this.staff = staff;
  }

  @Override
  public void run() {
    String path = "output/auth/" + staff.getNickname() + "/.htpasswd";

    try {

      String md5Psw = Md5Crypt.md5Crypt(staff.getPassword().getBytes());

      PrintWriter pWriter = new PrintWriter(path);

      pWriter.println(staff.getNickname() + ":" + md5Psw);

      pWriter.close();
    }

    catch (IOException e) {
      e.printStackTrace();
    }

  }

  public Staff getStaff() {
    return this.staff;
  }

  public void setStaff(Staff staff) {
    this.staff = staff;
  }

}