package com.gosecuri;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.staff.Staff;

public class Main {

  /**
   * 
   * @param args
   * @
   */
  public static void main(String[] args) {
    String pathStaff = "assets/staff.txt";
    try {
      List<String> agentsFiles = returnPaths(pathStaff);

      List<Staff> staffs = new ArrayList<>();

      for (String agentFile : agentsFiles) {
        Staff agent = returnStaff(agentFile);
        staffs.add(agent);
      }

      generateHTML index = new generateHTML();

      index.createIndex(staffs);

      for (Staff staff : staffs) {
        generateHTML g = new generateHTML(staff);
        g.run();
        createHtpasswd ht = new createHtpasswd(staff);
        ht.run();
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * 
   * @param pathStaff
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   */
  private static List<String> returnPaths(String pathStaff) throws FileNotFoundException, IOException {
    BufferedReader buffer = new BufferedReader(new FileReader(pathStaff));
    List<String> agentFiles = new ArrayList<>();
    while (buffer.ready()) {
      String line = buffer.readLine();
      agentFiles.add("assets/agents/" + line + ".txt");
    }
    buffer.close();
    return agentFiles;
  }

  /**
   * 
   * @param pathAgent
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   */
  private static Staff returnStaff(String pathAgent) throws FileNotFoundException, IOException {
    BufferedReader buffer = new BufferedReader(new FileReader(pathAgent));
    Staff agent;
    List<String> file = new ArrayList<>();
    List<String> items = new ArrayList<>();
    while (buffer.ready()) {
      String line = buffer.readLine();
      file.add(line);
    }
    buffer.close();

    for (int i = 5; i < file.size(); i++) {
      items.add(file.get(i));
    }
    agent = new Staff(file.get(1), file.get(0), file.get(2), file.get(3), items);
    return agent;
  }

}
