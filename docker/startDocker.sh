CONTAINER_NAME=nginx_mspr_java
OLD_CONTAINER_ID=$(docker ps -a | grep ${CONTAINER_NAME} | cut -d' ' -f1)
WEBCONTENT="$(pwd)/output/webcontent"
NGINX_CONF="$(pwd)/docker/nginx/"
AUTH_PATH="$(pwd)/output/auth"

echo $OLD_CONTAINER_ID
if [ -n "$OLD_CONTAINER_ID" ]; then
  docker rm -f $OLD_CONTAINER_ID
fi
docker run -d --name $CONTAINER_NAME -p 8081:80 -v $WEBCONTENT:/var/www/html -v $NGINX_CONF:/etc/nginx/conf.d/ -v $AUTH_PATH:/etc/nginx/auth/ nginx:1.18
NEW_CONTAINER_ID=$(docker ps -a | grep ${CONTAINER_NAME} | cut -d' ' -f1)
#docker exec -it "$NEW_CONTAINER_ID" bash
exit 0
